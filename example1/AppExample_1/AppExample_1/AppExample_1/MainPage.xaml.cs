﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppExample_1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        int count = 0, count2 = 0;
        private void btn_Click(object sender, EventArgs e)
        {
            count++;
            ((Button)sender).Text = $"You click {count} times.";
        }

        private void btn_Click2(object sender, EventArgs e)
        {
            count2++;
            X:l1.Text = $"You click {count2} times.";
        }

        private async void btn_Next(object sender, EventArgs e)
        {
            //var page = new TabbedPage1();
            //NavigationPage TabbedPage1 = new NavigationPage(new TabbedPage1());
            await Navigation.PushModalAsync(new TabbedPage1());
        }
    }
}
